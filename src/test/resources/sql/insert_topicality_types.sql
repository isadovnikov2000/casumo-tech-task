INSERT INTO public.topicality_types (id, name, price, points, discount_days_count)
VALUES ('128edd25-c999-4a33-a1e7-6bac9e195e3a', 'New releases', 40, 2, 1);
INSERT INTO public.topicality_types (id, name, price, points, discount_days_count)
VALUES ('228edd25-c999-4a33-a1e7-6bac9e195e3a', 'Regular films', 30, 1, 3);
INSERT INTO public.topicality_types (id, name, price, points, discount_days_count)
VALUES ('328edd25-c999-4a33-a1e7-6bac9e195e3a', 'Old film', 30, 1, 5);
