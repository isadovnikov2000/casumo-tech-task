package info.isadovnikov.techtest.users.clients

import info.isadovnikov.techtest.AbstractIntegrationTest
import org.hamcrest.Matchers
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.net.URI

@SqlGroup(
    Sql(value = [
        "classpath:sql/insert_clients.sql",
        "classpath:sql/insert_topicality_types.sql",
        "classpath:sql/insert_products.sql",
        "classpath:sql/insert_users.sql"],
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(value = ["classpath:sql/delete_all.sql"],
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
)
@AutoConfigureMockMvc
class ClientControllerIntegrationTest : AbstractIntegrationTest() {
    @Autowired
    lateinit var mvc: MockMvc


    @Test
    fun `Should return points for client if exist`() {
        //GIVEN
        val clientId = "528edc25-c999-4a33-a1e7-7bac9e195e3a"

        val expectedResult = ClientPointsResponse(
            0
        )

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/clients/$clientId/points"))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.points", Matchers.`is`(expectedResult.points.toInt())))
    }

}

