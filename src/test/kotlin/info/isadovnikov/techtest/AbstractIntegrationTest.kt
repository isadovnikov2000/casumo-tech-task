package info.isadovnikov.techtest

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.testcontainers.containers.GenericContainer
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

@RunWith(SpringRunner::class)
@SpringBootTest
@ActiveProfiles("test")
abstract class AbstractIntegrationTest {

    val mapper = objectMapper()

    private final fun objectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.registerModule(JavaTimeModule())
        val simpleModule = SimpleModule()
        simpleModule.addSerializer(OffsetDateTime::class.java, object : JsonSerializer<OffsetDateTime>() {
            override fun serialize(offsetDateTime: OffsetDateTime, jsonGenerator: JsonGenerator, serializerProvider: SerializerProvider) {
                jsonGenerator.writeString(DateTimeFormatter.ISO_DATE_TIME.format(offsetDateTime))
            }
        })
        objectMapper.registerModule(simpleModule)

        return objectMapper
    }

    companion object {
        const val TEST_USERNAME = "test"
        const val TEST_PASSWORD = "test"

        init {
            val mongoBasePort = 27017
            val mongo = KGenericContainer("mongo:3.4.21-xenial")
                .withExposedPorts(mongoBasePort)

            mongo.start()

            System.setProperty("spring.data.mongodb.port", mongo.getMappedPort(mongoBasePort).toString())
        }
    }

    private class KGenericContainer(imageName: String) : GenericContainer<KGenericContainer>(imageName)
}
