package info.isadovnikov.techtest.utils

import info.isadovnikov.techtest.orders.CreateOrderRequestDto
import info.isadovnikov.techtest.orders.CreateOrderResponceDto
import info.isadovnikov.techtest.orders.CreateOrdersRequestDto
import info.isadovnikov.techtest.orders.CreateOrdersResponceDto
import info.isadovnikov.techtest.orders.ProductDto
import info.isadovnikov.techtest.products.Product
import info.isadovnikov.techtest.products.ProductState
import info.isadovnikov.techtest.products.TopicalityType
import java.time.OffsetDateTime
import java.util.UUID

class TestUtils {
    companion object {
        fun createOrdersRequestDto(clientId: UUID, currentDateTime: OffsetDateTime): CreateOrdersRequestDto {
            val orders = createOrderRequestDtos(currentDateTime)

            return CreateOrdersRequestDto(
                orders,
                clientId
            )
        }

        fun createOrderRequestDtos(currentDateTime: OffsetDateTime): Set<CreateOrderRequestDto> {
            return setOf(
                CreateOrderRequestDto(
                    products = setOf(
                        UUID.fromString("129edc25-c999-4a33-a1e7-6bac9e195e3a")
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(1)
                ),
                CreateOrderRequestDto(
                    products = setOf(
                        UUID.fromString("238edc25-c999-4a33-a1e7-6bac9e195e3a")
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(5)
                ),
                CreateOrderRequestDto(
                    products = setOf(
                        UUID.fromString("329edc25-c999-4a33-a1e7-6bac9e195e3a")
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(2)
                ),
                CreateOrderRequestDto(
                    products = setOf(
                        UUID.fromString("322edc25-c999-4a33-a1e7-6bac9e195e3a")
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(7)
                )
            )
        }

        fun createOrdersResponceDto(clientId: UUID, currentDateTime: OffsetDateTime): CreateOrdersResponceDto {

            val orders = setOf(
                CreateOrderResponceDto(
                    products = setOf(
                        ProductDto(
                            UUID.fromString("129edc25-c999-4a33-a1e7-6bac9e195e3a"),
                            "Matrix 11"
                        )
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(1),
                    price = 40.0,
                    points = 2
                ),
                CreateOrderResponceDto(
                    products = setOf(
                        ProductDto(
                            UUID.fromString("238edc25-c999-4a33-a1e7-6bac9e195e3a"),
                            "Spider Man"
                        )
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(5),
                    price = 90.0,
                    points = 1
                ),
                CreateOrderResponceDto(
                    products = setOf(
                        ProductDto(
                            UUID.fromString("1329edc25-c999-4a33-a1e7-6bac9e195e3a"),
                            "Spider Man 2"
                        )
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(2),
                    price = 30.0,
                    points = 1
                ),
                CreateOrderResponceDto(
                    products = setOf(
                        ProductDto(
                            UUID.fromString("322edc25-c999-4a33-a1e7-6bac9e195e3a"),
                            "Out of Africa"
                        )
                    ),
                    from = currentDateTime,
                    to = currentDateTime.plusDays(7),
                    price = 90.0,
                    points = 1
                )
            )

            return CreateOrdersResponceDto(
                orders = orders,
                client = clientId,
                totalPrice = 250.0,
                points = 5
            )
        }

        fun products(): Set<Product> {
            val typeHigh = TopicalityType(
                id = UUID.randomUUID(),
                name = "type high",
                points = 5,
                price = 50.0,
                discountDaysCount = 1
            )
            val typeMedium = TopicalityType(
                id = UUID.randomUUID(),
                name = "type medium",
                points = 3,
                price = 25.0,
                discountDaysCount = 3
            )
            val typeLow = TopicalityType(
                id = UUID.randomUUID(),
                name = "type low",
                points = 1,
                price = 10.0,
                discountDaysCount = 5
            )
            return setOf(
                Product(
                    id = UUID.randomUUID(),
                    name = "product High 1",
                    state = ProductState.AVAILABLE,
                    topicalityType = typeHigh
                ),
                Product(
                    id = UUID.randomUUID(),
                    name = "product low 1",
                    state = ProductState.AVAILABLE,
                    topicalityType = typeLow
                ),
                Product(
                    id = UUID.randomUUID(),
                    name = "product medium 1",
                    state = ProductState.AVAILABLE,
                    topicalityType = typeMedium
                ),
                Product(
                    id = UUID.randomUUID(),
                    name = "product low 2",
                    state = ProductState.AVAILABLE,
                    topicalityType = typeLow
                ),
                Product(
                    id = UUID.randomUUID(),
                    name = "product high 2",
                    state = ProductState.AVAILABLE,
                    topicalityType = typeHigh
                ),
                Product(
                    id = UUID.randomUUID(),
                    name = "product medium 2",
                    state = ProductState.AVAILABLE,
                    topicalityType = typeMedium
                )
            )
        }
    }
}
