package info.isadovnikov.techtest.products

import info.isadovnikov.techtest.utils.TestUtils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.`when` as on

@RunWith(MockitoJUnitRunner::class)
class DefaultProductServiceTest {

    @Mock
    lateinit var productRepository: ProductRepository

    @Mock
    lateinit var topicalityTypeRepository: TopicalityTypeRepository

    @InjectMocks
    lateinit var productService: DefaultProductService

    @Test
    fun `Should return points for products`() {
        //GIVEN
        val products = TestUtils.products()
        val ids = products.map { it.id!! }

        on(productRepository.findAllById(ids)).thenReturn(products)

        //WHEN
        val actualPoints = productService.calculatePoint(ids)

        //THEN
        Assert.assertEquals(18, actualPoints)

    }


    @Test
    fun `Should return price for products`() {
        //GIVEN
        val products = TestUtils.products()
        val ids = products.map { it.id!! }

        on(productRepository.findAllById(ids)).thenReturn(products)

        //WHEN
        val actualPrice = productService.calculatePrice(ids, 5)

        //THEN
        Assert.assertEquals(670.0, actualPrice, 0.0)
    }

}
