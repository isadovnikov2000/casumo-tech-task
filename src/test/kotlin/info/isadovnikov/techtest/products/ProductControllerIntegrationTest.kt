package info.isadovnikov.techtest.products

import info.isadovnikov.techtest.AbstractIntegrationTest
import org.hamcrest.Matchers.`is`
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.net.URI
import java.util.UUID

@SqlGroup(
    Sql(value = [
        "classpath:sql/insert_clients.sql",
        "classpath:sql/insert_topicality_types.sql",
        "classpath:sql/insert_products.sql",
        "classpath:sql/insert_users.sql"],
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(value = ["classpath:sql/delete_all.sql"],
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
)
@AutoConfigureMockMvc
class ProductControllerIntegrationTest : AbstractIntegrationTest() {

    @Autowired
    lateinit var mvc: MockMvc

    @Test
    fun `Should create product and return it`() {
        //GIVEN
        val productName = "Test Product"
        val ow = mapper.writer().withDefaultPrettyPrinter()

        val expectedResult = ProductResponseDto(
            UUID.randomUUID(),
            productName,
            30.0,
            1,
            3
        )

        val product = CreateProductRequestDto(
            productName,
            UUID.fromString("228edd25-c999-4a33-a1e7-6bac9e195e3a")
        )
        val requestJson = ow.writeValueAsString(product)

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .post(URI("/v1/products"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isCreated)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.name", `is`(expectedResult.name)))
            .andExpect(jsonPath("$.price", `is`(expectedResult.price)))
            .andExpect(jsonPath("$.points", `is`(expectedResult.points.toInt())))
            .andExpect(jsonPath("$.discountDaysCount", `is`(expectedResult.discountDaysCount)))
    }

    @Test
    fun `Should return product if exist`() {
        //GIVEN
        val productId = UUID.fromString("238edc25-c999-4a33-a1e7-6bac9e195e3a")

        val expectedResult = ProductResponseDto(
            productId,
            "Spider Man",
            30.0,
            1,
            3
        )

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/products/$productId"))
                .contentType(APPLICATION_JSON_UTF8)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", `is`(expectedResult.id.toString())))
            .andExpect(jsonPath("$.name", `is`(expectedResult.name)))
            .andExpect(jsonPath("$.price", `is`(expectedResult.price)))
            .andExpect(jsonPath("$.points", `is`(expectedResult.points.toInt())))
            .andExpect(jsonPath("$.discountDaysCount", `is`(expectedResult.discountDaysCount)))
    }

    @Test
    fun `Should delete product`() {
        //GIVEN
        val productId = UUID.fromString("238edc25-c999-4a33-a1e7-6bac9e195e3a")

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .delete(URI("/v1/products/$productId"))
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isOk)
    }
}
