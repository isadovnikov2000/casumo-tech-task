package info.isadovnikov.techtest.orders

import info.isadovnikov.techtest.orders.history.OrderHistory
import info.isadovnikov.techtest.orders.history.OrderHistoryRepository
import info.isadovnikov.techtest.orders.history.OrderProductHistory
import info.isadovnikov.techtest.products.ProductService
import info.isadovnikov.techtest.users.clients.Client
import info.isadovnikov.techtest.users.clients.ClientRepository
import info.isadovnikov.techtest.utils.TestUtils
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.junit.MockitoJUnitRunner
import java.time.OffsetDateTime
import java.util.Date
import java.util.Optional
import java.util.UUID
import org.mockito.Mockito.`when` as on

@RunWith(MockitoJUnitRunner::class)
class DefaultOrderServiceTest {

    @Mock
    lateinit var orderRepository: OrderRepository

    @Mock
    lateinit var clientRepository: ClientRepository

    @Mock
    lateinit var productService: ProductService

    @Mock
    lateinit var orderHistoryRepository: OrderHistoryRepository

    @InjectMocks
    lateinit var defaultOrderService: DefaultOrderService

    val now: OffsetDateTime = OffsetDateTime.now()

    val clientId: UUID = UUID.randomUUID()

    @After
    fun after() {
        verifyNoMoreInteractions(orderRepository)
        verifyNoMoreInteractions(clientRepository)
        verifyNoMoreInteractions(productService)
        verifyNoMoreInteractions(orderHistoryRepository)
    }

    @Test
    fun `Should create order`() {
        //GIVEN
        val products = TestUtils.products()
        val projectIds = products.map { it.id!! }.toSet()
        val clientName = UUID.randomUUID().toString()
        val client = Client(clientId, clientName, 0)
        val from = now
        val to = now.plusDays(10)
        val state = OrderState.OPEN
        val orderId = UUID.randomUUID()
        val order = Order(
            products = products,
            totalPrice = 0.0,
            points = 0,
            client = client,
            from = from,
            to = to,
            state = state
        )
        val productsHistory = order.products.map { product ->
            OrderProductHistory(
                product.id.toString(),
                product.name,
                product.topicalityType.price,
                product.topicalityType.name,
                product.topicalityType.points,
                product.topicalityType.discountDaysCount.toLong()
            )
        }.toSet()
        val orderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            order.state,
            order.points
        )
        val captor = ArgumentCaptor.forClass(OrderHistory::class.java)

        on(productService.findByIds(projectIds)).thenReturn(products)
        on(clientRepository.findById(clientId)).thenReturn(Optional.of(client))
        on(orderRepository.save(order)).thenReturn(order.copy(orderId))
        on(orderHistoryRepository.insert(captor.capture())).thenReturn(orderHistory)

        //WHEN
        val actualResult = defaultOrderService.createOrder(projectIds, from, to, clientId)

        //THEN
        assertNotNull(actualResult)
        assertEquals(order.copy(orderId), actualResult)

        verify(productService).findByIds(projectIds)
        verify(productService).calculatePoint(projectIds)
        verify(productService).calculatePrice(projectIds, 10)
        verify(orderRepository).save(order)
        verify(clientRepository).findById(clientId)
        verify(clientRepository).save(client)
        verify(orderHistoryRepository).insert(captor.capture())
    }

    @Test
    fun `Should return order with additional price`() {
        //GIVEN
        val products = TestUtils.products()
        val clientName = UUID.randomUUID().toString()
        val client = Client(clientId, clientName, 0)
        val from = now
        val to = now.plusDays(10)
        val state = OrderState.OPEN
        val orderId = UUID.randomUUID()
        val order = Order(
            id = orderId,
            products = products,
            totalPrice = 0.0,
            points = 0,
            client = client,
            from = from,
            to = to,
            state = state
        )
        val productsHistory = order.products.map { product ->
            OrderProductHistory(
                product.id.toString(),
                product.name,
                product.topicalityType.price,
                product.topicalityType.name,
                product.topicalityType.points,
                product.topicalityType.discountDaysCount.toLong()
            )
        }.toSet()
        val orderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            order.state,
            order.points
        )

        on(orderRepository.findFirstByIdAndStateNot(orderId, OrderState.DELETE)).thenReturn(order)
        on(orderHistoryRepository.findFirstByOrderIdOrderByCreatedDateDesc(orderId)).thenReturn(orderHistory)

        //WHEN
        val actualResult = defaultOrderService.checkOrder(orderId)

        //THEN
        assertNotNull(actualResult)
        assertEquals(actualResult.totalPrice, 0.0, 0.0)

        verify(orderRepository).findFirstByIdAndStateNot(orderId, OrderState.DELETE)
        verify(orderHistoryRepository).findFirstByOrderIdOrderByCreatedDateDesc(orderId)
    }

    @Test
    fun `Should return order by id`() {
        //GIVEN
        val products = TestUtils.products().map {
            it.copy(topicalityType = it.topicalityType.copy(id = null))
        }.toSet()
        val clientName = UUID.randomUUID().toString()
        val client = Client(clientId, clientName, 0)
        val from = now
        val to = now.plusDays(10)
        val state = OrderState.OPEN
        val orderId = UUID.randomUUID()
        val order = Order(
            id = orderId,
            products = products,
            totalPrice = 0.0,
            points = 0,
            client = client,
            from = from,
            to = to,
            state = state
        )
        val productsHistory = order.products.map { product ->
            OrderProductHistory(
                product.id.toString(),
                product.name,
                product.topicalityType.price,
                product.topicalityType.name,
                product.topicalityType.points,
                product.topicalityType.discountDaysCount.toLong()
            )
        }.toSet()
        val orderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            order.state,
            order.points
        )

        on(orderRepository.findFirstByIdAndStateNot(orderId, OrderState.DELETE)).thenReturn(order)
        on(orderHistoryRepository.findFirstByOrderIdOrderByCreatedDateDesc(orderId)).thenReturn(orderHistory)

        //WHEN
        val actualResult = defaultOrderService.getOrder(orderId)

        //THEN
        assertNotNull(actualResult)
        assertEquals(actualResult, order)

        verify(orderRepository).findFirstByIdAndStateNot(orderId, OrderState.DELETE)
        verify(orderHistoryRepository).findFirstByOrderIdOrderByCreatedDateDesc(orderId)
    }

    @Test
    fun `Should mark order as deleted`() {
        //GIVEN
        val products = TestUtils.products().map {
            it.copy(topicalityType = it.topicalityType.copy(id = null))
        }.toSet()
        val clientName = UUID.randomUUID().toString()
        val client = Client(clientId, clientName, 0)
        val from = now
        val to = now.plusDays(10)
        val state = OrderState.OPEN
        val orderId = UUID.randomUUID()
        val order = Order(
            id = orderId,
            products = products,
            totalPrice = 0.0,
            points = 0,
            client = client,
            from = from,
            to = to,
            state = state
        )
        val productsHistory = order.products.map { product ->
            OrderProductHistory(
                product.id.toString(),
                product.name,
                product.topicalityType.price,
                product.topicalityType.name,
                product.topicalityType.points,
                product.topicalityType.discountDaysCount.toLong()
            )
        }.toSet()
        val orderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            order.state,
            order.points
        )

        val deletedOrder = order.copy(state = OrderState.DELETE)
        val deletedOrderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            OrderState.DELETE,
            order.points
        )

        val captor = ArgumentCaptor.forClass(OrderHistory::class.java)

        on(orderRepository.findFirstByIdAndStateNot(orderId, OrderState.DELETE)).thenReturn(order)
        on(orderHistoryRepository.findFirstByOrderIdOrderByCreatedDateDesc(orderId)).thenReturn(orderHistory)
        on(orderRepository.save(deletedOrder)).thenReturn(deletedOrder)
        on(orderHistoryRepository.insert(captor.capture())).thenReturn(deletedOrderHistory)

        //WHEN
        defaultOrderService.deleteOrder(orderId)

        //THEN
        verify(orderRepository).findFirstByIdAndStateNot(orderId, OrderState.DELETE)
        verify(orderHistoryRepository).findFirstByOrderIdOrderByCreatedDateDesc(orderId)
        verify(orderRepository).save(deletedOrder)
        verify(orderHistoryRepository).insert(captor.capture())
    }

    @Test
    fun `Should mark order as closed`() {
        //GIVEN
        val products = TestUtils.products().map {
            it.copy(topicalityType = it.topicalityType.copy(id = null))
        }.toSet()
        val clientName = UUID.randomUUID().toString()
        val client = Client(clientId, clientName, 0)
        val from = now
        val to = now.plusDays(10)
        val state = OrderState.OPEN
        val orderId = UUID.randomUUID()
        val order = Order(
            id = orderId,
            products = products,
            totalPrice = 0.0,
            points = 0,
            client = client,
            from = from,
            to = to,
            state = state
        )
        val productsHistory = order.products.map { product ->
            OrderProductHistory(
                product.id.toString(),
                product.name,
                product.topicalityType.price,
                product.topicalityType.name,
                product.topicalityType.points,
                product.topicalityType.discountDaysCount.toLong()
            )
        }.toSet()
        val orderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            order.state,
            order.points
        )

        val closedOrder = order.copy(state = OrderState.CLOSE)
        val closedOrderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            OrderState.CLOSE,
            order.points
        )

        val captor = ArgumentCaptor.forClass(OrderHistory::class.java)

        on(orderRepository.findFirstByIdAndStateNot(orderId, OrderState.DELETE)).thenReturn(order)
        on(orderHistoryRepository.findFirstByOrderIdOrderByCreatedDateDesc(orderId)).thenReturn(orderHistory)
        on(orderRepository.save(closedOrder)).thenReturn(closedOrder)
        on(orderHistoryRepository.insert(captor.capture())).thenReturn(closedOrderHistory)

        //WHEN
        val actualResult = defaultOrderService.closeOrder(orderId)

        //THEN
        assertNotNull(actualResult)
        assertEquals(closedOrder, actualResult)
        verify(orderRepository).findFirstByIdAndStateNot(orderId, OrderState.DELETE)
        verify(orderHistoryRepository).findFirstByOrderIdOrderByCreatedDateDesc(orderId)
        verify(orderRepository).save(closedOrder)
        verify(orderHistoryRepository).insert(captor.capture())
    }

    //TODO add exceptions tests
}
