package info.isadovnikov.techtest.orders

import info.isadovnikov.techtest.AbstractIntegrationTest
import info.isadovnikov.techtest.utils.TestUtils
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.hasSize
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.net.URI
import java.time.OffsetDateTime
import java.util.UUID

@SqlGroup(
    Sql(value = [
        "classpath:sql/insert_clients.sql",
        "classpath:sql/insert_topicality_types.sql",
        "classpath:sql/insert_products.sql",
        "classpath:sql/insert_users.sql"],
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(value = ["classpath:sql/delete_all.sql"],
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
)
@AutoConfigureMockMvc
class OrderCreateIntegrationTest : AbstractIntegrationTest() {

    @Autowired
    lateinit var mvc: MockMvc

    val clientId: UUID = UUID.fromString("528edc25-c999-4a33-a1e7-7bac9e195e3a")

    val now: OffsetDateTime = OffsetDateTime.now()

    @Test
    fun `Should create orders and return order summary`() {
        //GIVEN
        val ow = mapper.writer().withDefaultPrettyPrinter()

        val expectedResult = TestUtils.createOrdersResponceDto(clientId, now)

        val orders = TestUtils.createOrdersRequestDto(clientId, now)
        val requestJson = ow.writeValueAsString(orders)

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .post(URI("/v1/orders"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isCreated)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.orders", hasSize<Any>(expectedResult.orders.size)))
            .andExpect(jsonPath("$.totalPrice", `is`(expectedResult.totalPrice)))
            .andExpect(jsonPath("$.points", `is`(expectedResult.points.toInt())))
            .andExpect(jsonPath("$.client", `is`(clientId.toString())))
    }

    @Test
    fun `Should return 404 status when client do not exist`() {
        //GIVEN
        val ow = mapper.writer().withDefaultPrettyPrinter()

        val orders = CreateOrdersRequestDto(
            orders = TestUtils.createOrderRequestDtos(now),
            client = UUID.randomUUID()
        )
        val requestJson = ow.writeValueAsString(orders)

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .post(URI("/v1/orders"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isNotFound)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
    }

    @Test
    fun `Should return 404 status when products do not exist`() {
        //GIVEN
        val ow = mapper.writer().withDefaultPrettyPrinter()

        val orders = CreateOrdersRequestDto(
            orders = setOf(CreateOrderRequestDto(
                products = setOf(),
                from = now,
                to = now
            )),
            client = UUID.randomUUID()
        )
        val requestJson = ow.writeValueAsString(orders)

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .post(URI("/v1/orders"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isNotFound)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
    }

    @Test
    fun `Should return 400 status when to is before from`() {
        //GIVEN
        val ow = mapper.writer().withDefaultPrettyPrinter()

        val orders = CreateOrdersRequestDto(
            orders = setOf(CreateOrderRequestDto(
                products = setOf(),
                from = now.plusDays(1),
                to = now.minusDays(1)
            )),
            client = UUID.randomUUID()
        )
        val requestJson = ow.writeValueAsString(orders)

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .post(URI("/v1/orders"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isBadRequest)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
    }
}
