package info.isadovnikov.techtest.orders

import info.isadovnikov.techtest.AbstractIntegrationTest
import org.hamcrest.Matchers
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.net.URI
import java.time.OffsetDateTime
import java.util.UUID

@SqlGroup(
    Sql(value = [
        "classpath:sql/insert_clients.sql",
        "classpath:sql/insert_topicality_types.sql",
        "classpath:sql/insert_products.sql",
        "classpath:sql/insert_users.sql"],
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(value = ["classpath:sql/delete_all.sql"],
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
)
@AutoConfigureMockMvc
class OrderCheckIntegrationTest : AbstractIntegrationTest() {

    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var orderService: OrderService

    val clientId: UUID = UUID.fromString("528edc25-c999-4a33-a1e7-7bac9e195e3a")
    val now: OffsetDateTime = OffsetDateTime.now()

    @Test
    fun `Should return additional payment when order is delayed for product one`() {
        //GIVEN
        val order = orderService.createOrder(
            productIds = setOf(
                UUID.fromString("129edc25-c999-4a33-a1e7-6bac9e195e3a")
            ),
            from = now.minusDays(3),
            to = now.minusDays(2),
            clientId = clientId
        )

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/orders/${order.id}/check"))
                .contentType(APPLICATION_JSON_UTF8)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.additionalPrice", Matchers.`is`(80.0)))
    }

    @Test
    fun `Should return additional payment when order is delayed for product two`() {
        //GIVEN
        val order = orderService.createOrder(
            productIds = setOf(
                UUID.fromString("238edc25-c999-4a33-a1e7-6bac9e195e3a")
            ),
            from = now.minusDays(6),
            to = now.minusDays(1),
            clientId = clientId
        )

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/orders/${order.id}/check"))
                .contentType(APPLICATION_JSON_UTF8)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.additionalPrice", Matchers.`is`(30.0)))
    }

    @Test
    fun `Should return additional payment as zero when order is not delayed for product three`() {
        //GIVEN
        val order = orderService.createOrder(
            productIds = setOf(
                UUID.fromString("329edc25-c999-4a33-a1e7-6bac9e195e3a")
            ),
            from = now.minusDays(1),
            to = now.plusDays(1),
            clientId = clientId
        )

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/orders/${order.id}/check"))
                .contentType(APPLICATION_JSON_UTF8)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.additionalPrice", Matchers.`is`(0.0)))
    }

    @Test
    fun `Should return additional payment as zero when order is not delayed for product fore`() {
        //GIVEN
        val order = orderService.createOrder(
            productIds = setOf(
                UUID.fromString("322edc25-c999-4a33-a1e7-6bac9e195e3a")
            ),
            from = now.minusDays(6),
            to = now.plusDays(1),
            clientId = clientId
        )

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/orders/${order.id}/check"))
                .contentType(APPLICATION_JSON_UTF8)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isOk)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.additionalPrice", Matchers.`is`(0.0)))
    }

    @Test
    fun `Should return 404 status when order do not exist`() {
        //GIVEN
        val notExistId = UUID.randomUUID()

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get(URI("/v1/orders/${notExistId}/check"))
                .contentType(APPLICATION_JSON_UTF8)
                .with(httpBasic(TEST_USERNAME, TEST_PASSWORD))
        )

        //THEN
        resultActions.andExpect(status().isNotFound)
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
    }
}
