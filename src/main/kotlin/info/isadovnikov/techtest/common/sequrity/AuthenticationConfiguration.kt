package info.isadovnikov.techtest.common.sequrity

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.stereotype.Component

@Component
class AuthenticationConfiguration : GlobalAuthenticationConfigurerAdapter() {

    @Value("\${api.user.username}")
    private val apiUserUserName: String? = null

    @Value("\${api.user.password}")
    private val apiUserPassword: String? = null

    override fun init(auth: AuthenticationManagerBuilder) {
        val encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder()
        auth.inMemoryAuthentication()
            .passwordEncoder(encoder)
            .withUser(apiUserUserName)
            .password(encoder.encode(apiUserPassword))
            .roles(API_USER)
    }

    companion object {
        const val API_USER = "API_USER"
    }

}
