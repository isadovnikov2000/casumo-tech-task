package info.isadovnikov.techtest.common.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.OffsetDateTime
import javax.persistence.EntityNotFoundException

@ControllerAdvice
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(EntityNotFoundException::class)
    fun handleSupplyEntityNotFoundException(e: EntityNotFoundException): ResponseEntity<ErrorMessage> {
        return ResponseEntity(logAndConvert(e), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(Throwable::class)
    fun handleInternalServerError(e: Exception): ResponseEntity<ErrorMessage> {
        return ResponseEntity(logAndConvert(e), HttpStatus.BAD_REQUEST)
    }

   private fun getRequestUri(): String {
        if (RequestContextHolder.currentRequestAttributes() is ServletRequestAttributes) {
            return (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request.requestURI
        }
        return ""
    }

    private fun logAndConvert(throwable: Throwable?): ErrorMessage {
        logger.error("Error catch by the GlobalExceptionHandler", throwable)
        return ErrorMessage(
            url = getRequestUri(),
            time = OffsetDateTime.now(),
            message = throwable?.message ?: ""
        )
    }

    data class ErrorMessage(
        val url: String = "",
        val time: OffsetDateTime = OffsetDateTime.now(),
        val message: String = ""
    )
}
