package info.isadovnikov.techtest.products

import org.hibernate.annotations.GenericGenerator
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "products")
data class Product(
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    val id: UUID? = null,
    val name: String,

    @Enumerated(EnumType.STRING)
    val state: ProductState,
    @ManyToOne(targetEntity = TopicalityType::class)
    val topicalityType: TopicalityType
)

@Entity
@Table(name = "topicality_types")
data class TopicalityType(
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    val id: UUID? = null,

    val name: String,
    val price: Double, // I dont want to make it complex with BigMoney
    val points: Long,
    val discountDaysCount: Int
)

enum class ProductState {
    AVAILABLE,
    DELETE
}
