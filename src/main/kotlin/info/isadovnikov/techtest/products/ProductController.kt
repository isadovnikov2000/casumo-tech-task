package info.isadovnikov.techtest.products

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/v1/products")
class ProductController(
    private val productService: ProductService
) {

    @GetMapping("/{id}")
    fun getProduct(@PathVariable id: UUID): ProductResponseDto = productService.findById(id).let {
        ProductResponseDto(it.id!!, it.name, it.topicalityType.price, it.topicalityType.points, it.topicalityType.discountDaysCount)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createProduct(@RequestBody createBody: CreateProductRequestDto): ProductResponseDto = productService.createProduct(createBody.name, createBody.topicalityTypeId).let {
        ProductResponseDto(it.id!!, it.name, it.topicalityType.price, it.topicalityType.points, it.topicalityType.discountDaysCount)
    }

    @PutMapping
    fun updateProduct() {
        TODO("not implemented")
    }

    @DeleteMapping("/{id}")
    fun deleteProduct(@PathVariable id: UUID) {
        productService.deleteProduct(id)
    }
}
