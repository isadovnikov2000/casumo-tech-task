package info.isadovnikov.techtest.products

import java.util.UUID

data class ProductResponseDto(
    val id: UUID,
    val name: String,
    val price: Double,
    val points: Long,
    val discountDaysCount: Int
)


data class CreateProductRequestDto(
    val name: String,
    val topicalityTypeId: UUID
)
