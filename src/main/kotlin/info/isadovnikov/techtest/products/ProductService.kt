package info.isadovnikov.techtest.products

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.UUID
import javax.persistence.EntityNotFoundException

interface ProductService {
    fun createProduct(name: String, topicalityTypeId: UUID): Product
    fun updateProduct(): Product
    fun deleteProduct(id: UUID)
    fun findById(id: UUID): Product
    fun findByIds(ids: Collection<UUID>): Set<Product>
    fun calculatePoint(ids: Collection<UUID>): Long
    fun calculatePrice(ids: Collection<UUID>, daysCount: Int): Double
    fun calculatePrice(daysCount: Int, discountDaysCount: Int, dailyPrice: Double): Double
}

@Service
class DefaultProductService(
    private val productRepository: ProductRepository,
    private val topicalityTypeRepository: TopicalityTypeRepository
) : ProductService {

    override fun createProduct(name: String, topicalityTypeId: UUID): Product {
        val type = topicalityTypeRepository.findById(topicalityTypeId).orElseThrow { throw EntityNotFoundException("Product [$topicalityTypeId] not found") }

        val product = Product(name = name, state = ProductState.AVAILABLE, topicalityType = type)
        return productRepository.save(product)

    }

    override fun updateProduct(): Product {
        TODO("not implemented")
    }

    override fun deleteProduct(id: UUID) {
        val product = productRepository.findByIdOrNull(id) ?: return

        val updateProduct = product.copy(state = ProductState.DELETE)
        productRepository.save(updateProduct)
    }

    override fun findById(id: UUID): Product = productRepository.findById(id).orElseThrow { throw EntityNotFoundException("Product [$id] not found") }

    override fun findByIds(ids: Collection<UUID>): Set<Product> = productRepository.findAllById(ids).toSet()

    override fun calculatePoint(ids: Collection<UUID>): Long = productRepository.findAllById(ids)
        .map { it.topicalityType }
        .map { it.points }
        .sum()

    override fun calculatePrice(ids: Collection<UUID>, daysCount: Int): Double = productRepository.findAllById(ids)
        .map { calculatePrice(it.topicalityType, daysCount) }
        .sum()

    override fun calculatePrice(daysCount: Int, discountDaysCount: Int, dailyPrice: Double): Double {
        return if (daysCount - discountDaysCount <= 0) {
            dailyPrice
        } else {
            dailyPrice + (daysCount - discountDaysCount) * dailyPrice
        }
    }

    private fun calculatePrice(topicalityType: TopicalityType, daysCount: Int): Double {
        val dailyPrice = topicalityType.price
        val discountDaysCount = topicalityType.discountDaysCount

        return calculatePrice(daysCount, discountDaysCount, dailyPrice)
    }
}



