package info.isadovnikov.techtest.users.clients

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/v1/clients")
class ClientController(
    private val clientService: ClientService
)  {

    @GetMapping("/{id}/points")
    fun getPoints(@PathVariable id: UUID): ClientPointsResponse = ClientPointsResponse(clientService.findById(id).points)

}

data class ClientPointsResponse(
    val points: Long
)
