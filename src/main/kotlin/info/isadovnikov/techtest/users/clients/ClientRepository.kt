package info.isadovnikov.techtest.users.clients

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ClientRepository : CrudRepository<Client, UUID>
