package info.isadovnikov.techtest.users.clients

import org.springframework.stereotype.Service
import java.util.UUID
import javax.persistence.EntityNotFoundException

interface ClientService  {
    fun findById(id: UUID): Client
}

@Service
class DefaultClientService(
    private val clientRepository: ClientRepository
): ClientService {
    override fun findById(id: UUID): Client = clientRepository.findById(id).orElseThrow { throw EntityNotFoundException("Client [$id] not found") }
}
