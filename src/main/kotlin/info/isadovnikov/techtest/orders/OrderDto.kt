package info.isadovnikov.techtest.orders

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.format.annotation.DateTimeFormat
import java.time.OffsetDateTime
import java.util.UUID


data class CreateOrdersRequestDto(
    val orders: Set<CreateOrderRequestDto>,
    val client: UUID
)

data class CreateOrderRequestDto(
    val products: Set<UUID>,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val from: OffsetDateTime,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val to: OffsetDateTime
)

data class CreateOrdersResponceDto(
    val orders: Set<CreateOrderResponceDto>,
    val client: UUID,
    val totalPrice: Double,
    val points: Long
)

data class CreateOrderResponceDto(
    val products: Set<ProductDto>,

    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val from: OffsetDateTime,

    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val to: OffsetDateTime,
    val price: Double,
    val points: Long
)

data class OrderResponceDto(
    val products: Set<ProductDto>,

    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val from: OffsetDateTime,

    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val to: OffsetDateTime,
    val price: Double,
    val points: Long,
    val client: UUID,
    val state: String
)

data class ProductDto(
    val id: UUID,
    val name: String
)

data class CheckOrderResponseDto(
    val products: Set<ProductDto>,
    val additionalPrice: Double
)
