package info.isadovnikov.techtest.orders.history;

import info.isadovnikov.techtest.orders.OrderState;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "order_history")
public class OrderHistory
{
    @Id
    private String id;
    private UUID orderId;
    private Date createdDate;
    private String createdBy;
    private Set<OrderProductHistory> products;
    private Double price;
    private Date from;
    private Date to;
    private String clientId;

    @Enumerated(EnumType.STRING)
    private OrderState state;
    private Long points;


    public OrderHistory(
        String id,
        UUID orderId,
        Date createdDate,
        String createdBy, Set<OrderProductHistory> products,
        Double price,
        Date from,
        Date to,
        String clientId,
        OrderState state, Long points)
    {
        this.id = id;
        this.orderId = orderId;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.products = products;
        this.price = price;
        this.from = from;
        this.to = to;
        this.clientId = clientId;
        this.state = state;
        this.points = points;
    }

    public String getId()
    {
        return id;
    }


    public void setId(String id)
    {
        this.id = id;
    }


    public Set<OrderProductHistory> getProducts()
    {
        return products;
    }


    public void setProducts(Set<OrderProductHistory> products)
    {
        this.products = products;
    }


    public Double getPrice()
    {
        return price;
    }


    public void setPrice(Double price)
    {
        this.price = price;
    }


    public Date getFrom()
    {
        return from;
    }


    public void setFrom(Date from)
    {
        this.from = from;
    }


    public Date getTo()
    {
        return to;
    }


    public void setTo(Date to)
    {
        this.to = to;
    }


    public String getClientId()
    {
        return clientId;
    }


    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }


    public OrderState getState()
    {
        return state;
    }


    public void setState(OrderState state)
    {
        this.state = state;
    }


    public Long getPoints()
    {
        return points;
    }


    public void setPoints(Long points)
    {
        this.points = points;
    }


    public UUID getOrderId()
    {
        return orderId;
    }


    public void setOrderId(UUID orderId)
    {
        this.orderId = orderId;
    }


    public Date getCreatedDate()
    {
        return createdDate;
    }


    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        OrderHistory that = (OrderHistory) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(orderId, that.orderId) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(products, that.products) &&
            Objects.equals(price, that.price) &&
            Objects.equals(from, that.from) &&
            Objects.equals(to, that.to) &&
            Objects.equals(clientId, that.clientId) &&
            state == that.state &&
            Objects.equals(points, that.points);
    }


    @Override
    public int hashCode()
    {
        return Objects.hash(id, orderId, products, price, from, to, clientId, state, points);
    }
}
