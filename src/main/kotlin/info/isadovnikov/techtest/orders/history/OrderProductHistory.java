package info.isadovnikov.techtest.orders.history;

import java.util.Objects;

public class OrderProductHistory
{
    private String id;
    private String name;
    private Double price;
    private String topicalityType;
    private Long points;
    private Long discountDaysCount;

    public OrderProductHistory(String id, String name, Double price, String topicalityType, Long points, Long discountDaysCount)
    {
        this.id = id;
        this.name = name;
        this.price = price;
        this.topicalityType = topicalityType;
        this.points = points;
        this.discountDaysCount = discountDaysCount;
    }


    public String getId()
    {
        return id;
    }


    public void setId(String id)
    {
        this.id = id;
    }


    public String getName()
    {
        return name;
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public Double getPrice()
    {
        return price;
    }


    public void setPrice(Double price)
    {
        this.price = price;
    }


    public String getTopicalityType()
    {
        return topicalityType;
    }


    public void setTopicalityType(String topicalityType)
    {
        this.topicalityType = topicalityType;
    }


    public Long getPoints()
    {
        return points;
    }


    public void setPoints(Long points)
    {
        this.points = points;
    }


    public Long getDiscountDaysCount()
    {
        return discountDaysCount;
    }


    public void setDiscountDaysCount(Long discountDaysCount)
    {
        this.discountDaysCount = discountDaysCount;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        OrderProductHistory that = (OrderProductHistory) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(price, that.price) &&
            Objects.equals(topicalityType, that.topicalityType) &&
            Objects.equals(points, that.points) &&
            Objects.equals(discountDaysCount, that.discountDaysCount);
    }


    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, price, topicalityType, points, discountDaysCount);
    }
}
