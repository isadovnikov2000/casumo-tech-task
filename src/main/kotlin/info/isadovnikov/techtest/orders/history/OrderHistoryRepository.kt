package info.isadovnikov.techtest.orders.history

import org.springframework.data.mongodb.repository.MongoRepository
import java.util.UUID

interface OrderHistoryRepository : MongoRepository<OrderHistory, String> {

    fun findFirstByOrderIdOrderByCreatedDateDesc(orderId: UUID): OrderHistory?

}

