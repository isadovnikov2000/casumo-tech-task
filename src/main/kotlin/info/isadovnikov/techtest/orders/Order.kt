package info.isadovnikov.techtest.orders

import info.isadovnikov.techtest.products.Product
import info.isadovnikov.techtest.users.clients.Client
import org.hibernate.annotations.GenericGenerator
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener::class)
data class Order(
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    val id: UUID? = null,

    @ManyToMany(targetEntity = Product::class)
    @JoinTable(
        name = "orders_products",
        joinColumns = [JoinColumn(
            name = "order_id", referencedColumnName = "id"
        )],
        inverseJoinColumns = [JoinColumn(
            name = "product_id", referencedColumnName = "id"
        )]
    )
    val products: Set<Product>,
    val totalPrice: Double,

    @Column(name = "from_date")
    val from: OffsetDateTime,

    @Column(name = "to_date")
    val to: OffsetDateTime,

    @ManyToOne(targetEntity = Client::class)
    val client: Client,

    @Enumerated(EnumType.STRING)
    val state: OrderState,
    val points: Long,

    @CreatedBy
    val createdBy: String? = null,

    @LastModifiedBy
    val modifiedBy: String? = null,

    @CreatedDate
    val createdDate: LocalDateTime? = null,

    @LastModifiedDate
    val modifiedDate: LocalDateTime? = null
)

enum class OrderState{
    OPEN, CLOSE, DELETE
}
