package info.isadovnikov.techtest.orders

import info.isadovnikov.techtest.orders.history.OrderHistory
import info.isadovnikov.techtest.orders.history.OrderHistoryRepository
import info.isadovnikov.techtest.orders.history.OrderProductHistory
import info.isadovnikov.techtest.products.Product
import info.isadovnikov.techtest.products.ProductService
import info.isadovnikov.techtest.products.ProductState
import info.isadovnikov.techtest.products.TopicalityType
import info.isadovnikov.techtest.users.clients.ClientRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.OffsetDateTime
import java.util.Date
import java.util.UUID
import javax.persistence.EntityNotFoundException
import javax.transaction.Transactional

interface OrderService {
    fun createOrder(productIds: Set<UUID>, from: OffsetDateTime, to: OffsetDateTime, clientId: UUID): Order
    fun checkOrder(orderId: UUID): Order
    fun closeOrder(orderId: UUID): Order
    fun getOrder(orderId: UUID): Order
    fun deleteOrder(orderId: UUID)
}

@Service
class DefaultOrderService(
    private val orderRepository: OrderRepository,
    private val clientRepository: ClientRepository,
    private val productService: ProductService,
    private val orderHistoryRepository: OrderHistoryRepository
) : OrderService {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Transactional
    override fun createOrder(productIds: Set<UUID>, from: OffsetDateTime, to: OffsetDateTime, clientId: UUID): Order {
        logger.debug("Create Client [$clientId] Order with products [$productIds] from [$from] to [$to]")

        if (to.isBefore(from)) {
            throw IllegalArgumentException("Order start date [$to] can not be before order end date [$from]")
        }

        val products = productService.findByIds(productIds).also { if (it.isEmpty()) throw EntityNotFoundException("Products [$productIds] not found") }
        val client = clientRepository.findById(clientId).orElseThrow { throw EntityNotFoundException("Client [$clientId] not found") }
        val points = productService.calculatePoint(productIds)

        val daysCount = to.dayOfYear - from.dayOfYear
        val price = productService.calculatePrice(productIds, daysCount)

        val order = Order(
            products = products,
            client = client,
            from = from,
            to = to,
            state = OrderState.OPEN,
            points = points,
            totalPrice = price
        )

        return orderRepository.save(order).also {
            saveOrderHistory(it)
        }.also {
            clientRepository.save(client.copy(points = client.points + points))
        }
    }

    override fun checkOrder(orderId: UUID): Order {
        val order = this.getOrder(orderId)

        val to = OffsetDateTime.now()

        val price = if (to.isAfter(order.to)) {
            val daysCount = to.dayOfYear - order.from.dayOfYear
            order.products.map { it.topicalityType }
                .map { productService.calculatePrice(daysCount, it.discountDaysCount, it.price) }
                .sum()
        } else {
            order.totalPrice
        }

        var totalPrice = price - order.totalPrice
        if(totalPrice < 0) {
            logger.error("Price calculated wrongly for order [$orderId]")
            totalPrice = 0.0
            //TODO send notification to admin
        }

        return order.copy(totalPrice = totalPrice)
    }

    override fun getOrder(orderId: UUID): Order {
        val order = orderRepository.findFirstByIdAndStateNot(orderId, OrderState.DELETE) ?: throw EntityNotFoundException("Order [$orderId] not found")

        val orderHistory = orderHistoryRepository.findFirstByOrderIdOrderByCreatedDateDesc(order.id!!) ?: throw EntityNotFoundException("Order [$orderId] not found")

        val products = orderHistory.products.map {
            Product(
                id = UUID.fromString(it.id),
                name = it.name,
                topicalityType = TopicalityType(
                    name = it.topicalityType,
                    price = it.price,
                    points = it.points,
                    discountDaysCount = it.discountDaysCount.toInt()
                ),
                state = ProductState.AVAILABLE
            )
        }.toSet()

        return order.copy(products = products)
    }

    @Transactional
    override fun deleteOrder(orderId: UUID) {
        logger.debug("Delete order [$orderId]")
        val order = this.getOrder(orderId)

        val updateOrder = order.copy(state =  OrderState.DELETE)

        orderRepository.save(updateOrder).also {
            saveOrderHistory(it)
        }
    }

    @Transactional
    override fun closeOrder(orderId: UUID): Order {
        logger.debug("Delete order [$orderId]")
        val order = this.checkOrder(orderId)

        val updateOrder = order.copy(state =  OrderState.CLOSE)

        return orderRepository.save(updateOrder).also {
            saveOrderHistory(it)
        }
    }

    private fun saveOrderHistory(order: Order) {
        val productsHistory = order.products.map { product ->
            OrderProductHistory(
                product.id.toString(),
                product.name,
                product.topicalityType.price,
                product.topicalityType.name,
                product.topicalityType.points,
                product.topicalityType.discountDaysCount.toLong()
            )
        }.toSet()
        val orderHistory = OrderHistory(
            UUID.randomUUID().toString(),
            order.id,
            Date(),
            order.createdBy,
            productsHistory,
            order.totalPrice,
            Date(order.from.toInstant().toEpochMilli()),
            Date(order.to.toInstant().toEpochMilli()),
            order.client.id.toString(),
            order.state,
            order.points
        )
        orderHistoryRepository.insert(orderHistory)
    }

}
