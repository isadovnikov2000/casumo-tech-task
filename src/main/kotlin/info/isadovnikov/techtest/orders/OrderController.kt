package info.isadovnikov.techtest.orders

import info.isadovnikov.techtest.products.Product
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException
import java.util.UUID

@RestController
@RequestMapping("/v1/orders")
class OrderController(
    private val orderService: OrderService
) {

    @GetMapping("/{id}")
    fun getOrder(@PathVariable id: UUID): OrderResponceDto = orderService.getOrder(id).let { order ->
        OrderResponceDto(
            toProductDto(order.products),
            order.from,
            order.to,
            order.totalPrice,
            order.points,
            order.client.id!!,
            order.state.name
        )
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createOrder(@RequestBody orderRequest: CreateOrdersRequestDto): CreateOrdersResponceDto {

        if (orderRequest.orders.isEmpty()) {
            throw HttpClientErrorException(HttpStatus.BAD_REQUEST, "Orders can not be empty")
        }

        return orderRequest.orders.map {
            orderService.createOrder(it.products, it.from, it.to, orderRequest.client)
        }.map { order ->
            CreateOrderResponceDto(
                toProductDto(order.products),
                order.from,
                order.to,
                order.totalPrice,
                order.points
            )
        }.toSet().let { orders ->
            CreateOrdersResponceDto(
                orders,
                orderRequest.client,
                orders.map { it.price }.sum(),
                orders.map { it.points }.sum()
            )
        }
    }

    @GetMapping("/{id}/check")
    fun checkOrder(@PathVariable id: UUID): CheckOrderResponseDto {
        return orderService.checkOrder(id).let { order ->
            CheckOrderResponseDto(
                products = toProductDto(order.products),
                additionalPrice = order.totalPrice
            )
        }
    }

    @PutMapping("/{id}/close")
    fun closeOrder(@PathVariable id: UUID): OrderResponceDto = orderService.closeOrder(id).let { order ->
        OrderResponceDto(
            toProductDto(order.products),
            order.from,
            order.to,
            order.totalPrice,
            order.points,
            order.client.id!!,
            order.state.name
        )
    }

    @DeleteMapping("/{id}")
    fun deleteOrder(@PathVariable id: UUID) = orderService.deleteOrder(id)

    private fun toProductDto(products: Collection<Product>) = products.map { ProductDto(it.id!!, it.name) }.toSet()

}



