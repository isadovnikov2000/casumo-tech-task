package info.isadovnikov.techtest.orders

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderRepository : CrudRepository<Order, UUID> {
    fun findFirstByIdAndStateNot(id: UUID, state: OrderState): Order?
}
